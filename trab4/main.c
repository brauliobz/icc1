/*
 *     ____  ____  ____
 *    / __ \/ __ \/ __ \
 *   / /  \/ /  \/ /  \ \
 *   \ \  / /\  / /\  / /
 *    \ \/ /\ \/ /\ \/ /
 *     \ \/  \ \/  \ \/
 *     /\ \  /\ \  /\ \
 *    / /\ \/ /\ \/ /\ \
 *   / /  \/ /  \/ /  \ \
 *  / /   / /\  / /\   \ \
 *  | |  / / /  \ \ \  | |
 *  \ \__\/ /    \/ /__/ /
 *   \_____/\____/ /____/
 *         \______/
 *
 * Escalonamento, retas e planos
 *
 * Autor: Bráulio Bezerra da Silva - BCC ICMC
 * Número USP: 10734540
 *
 * Trabalho 4 de ICC1 - 2018.1
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/** Número racional na forma p / q. */
typedef struct {
    int p, q;
} Rac;

// Operações com racionais

Rac  sub_rac(Rac x, Rac y);
Rac  mul_rac(Rac x, Rac y);
Rac  div_rac(Rac x, Rac y);
void print_rac(Rac x);
void print_matriz_rac(Rac **matriz, int linhas, int cols);

/** Plano na forma de equação geral: c[0]x + c[1]y + c[2]z + c[3] = 0. */
typedef struct {
    Rac c[4];
} Plano;

Plano ler_plano();

/** Reta definida por dois planos. */
typedef struct {
    Plano plano1, plano2;
} Reta ;

Reta ler_reta();

bool escalonar(Rac **matriz, int lins, int cols, int lin_inicial, int col_inicial);

int main() {

    // matriz, no formato de lista de linhas, para permitir fácil swap de linhas

    Rac **matriz = calloc(4, sizeof(Rac*));
    int qtd_linhas = 0;

    // lê as retas e planos e coloca seus coeficientes em 'matriz'

    scanf("%*d %*d");

    for (int i = 0; i < 2; i++) {
        char tipo_objeto;
        scanf("%*[^rp]"); // ignora tudo até chegar no 'r' ou 'c'
        scanf("%c", &tipo_objeto);

        if (tipo_objeto == 'r') {
            Reta r = ler_reta();

            matriz[qtd_linhas] = calloc(4, sizeof(Rac));
            memcpy(matriz[qtd_linhas], r.plano1.c, 4 * sizeof(Rac));
            qtd_linhas++;

            matriz[qtd_linhas] = calloc(4, sizeof(Rac));
            memcpy(matriz[qtd_linhas], r.plano2.c, 4 * sizeof(Rac));
            qtd_linhas++;
        } else {
            Plano p = ler_plano();

            matriz[qtd_linhas] = calloc(4, sizeof(Rac));
            memcpy(matriz[qtd_linhas], p.c, 4 * sizeof(Rac));
            qtd_linhas++;
        }
    }

    // realiza o escalonamento de 'matriz' para verificar se houve colisões

    bool absurdo = escalonar(matriz, qtd_linhas, 4, 0, 0);

    // resultados

    if (absurdo) {
        puts("nao");
    } else {
        puts("sim");
    }

    print_matriz_rac(matriz, qtd_linhas, 4);

    // liberação de memória

    for (int i = 0; i < qtd_linhas; i++) {
        free(matriz[i]);
    }
    free(matriz);

    return 0;
}

/**
 * Realiza a Eliminação de Gauss na matriz passada, iniciando na posição
 * (lin_inicial, col_inicial).
 *
 * Para escalonar toda uma matriz, passe 0 em lin_inicial e lin_final.
 *
 * @param matriz no formato de vetor de ponteiros para as linhas
 * @param lins qtd total de linhas da matriz
 * @param cols qtd total de colunas da matriz
 * @param lin_inicial linha inicial de escalonamento
 * @param col_inicial coluna inicial de escalonamento
 *
 * @return true se alguma das linhas da matriz escalonada, quando interpretada
 *              como uma equação de um sistema, não tiver solução
 */
bool escalonar(Rac **matriz, int lins, int cols, int lin_inicial, int col_inicial) {

    // procura pivot e faz swap, se necessário

    bool achou_pivot = true;
    if (matriz[lin_inicial][col_inicial].p == 0) {
        int lin_swap = 0;
        for (int i = lin_inicial + 1; i < lins; i++) {
            if (matriz[i][col_inicial].p != 0) {
                lin_swap = i;
                break;
            }
        }

        if (lin_swap != 0) {
            Rac* tmp = matriz[lin_inicial];
            matriz[lin_inicial] = matriz[lin_swap];
            matriz[lin_swap] = tmp;

        } else {
            achou_pivot = false;
        }
    }

    // pivoteamento

    if (achou_pivot) {
        Rac pivot = matriz[lin_inicial][col_inicial];
        for (int i = lin_inicial + 1; i < lins; i++) {
            if (matriz[i][col_inicial].p != 0) {
                Rac razao = div_rac(matriz[i][col_inicial], pivot);
                for (int j = col_inicial; j < cols; j++) {
                    matriz[i][j] = sub_rac(
                            matriz[i][j],
                            mul_rac(razao, matriz[lin_inicial][j]));
                }
            }
        }
    }

    // recursão para escalonamento do resto da matriz

    bool absurdo;

    if (achou_pivot) {
        if (lin_inicial < lins - 1 && col_inicial < cols - 1) {
            // achou um pivot, segue para as próximas linha e coluna
            absurdo = escalonar(matriz, lins, cols, lin_inicial + 1, col_inicial + 1);
        } else {
            absurdo = col_inicial == cols - 1;
        }
    } else {
        if (col_inicial < cols - 1) {
            // não achou um pivot, mantém a linha e segue para a próxima coluna
            absurdo = escalonar(matriz, lins, cols, lin_inicial, col_inicial + 1);
        } else {
            absurdo = matriz[lin_inicial][col_inicial].p != 0;
        }
    }

    return absurdo;
}

int mmc(int a, int b);

/** Subtração de racionais. Normaliza para o mínimo denominador comum. */
Rac sub_rac(const Rac x, const Rac y) {

    Rac s;

    if (y.p == 0) { // x = 0
        return x;
    } else if (x.p == 0) { // y = 0
        s = y;
        s.p = -s.p;
        return s;
    } else if (x.q == y.q) { // mesmo denominador
        s.p = x.p - y.p;
        s.q = x.q;
    } else { // denominadores diferentes
        int m = mmc(x.q, y.q);
        s.q = m;
        s.p = x.p * (m / x.q) - y.p * (m / y.q);
    }

    return s;
}

/** Realiza a multiplicação racional de x por y. */
Rac mul_rac(const Rac x, const Rac y) {
    Rac m;
    m.p = x.p * y.p;
    m.q = x.q * y.q;
    return m;
}

/** Realiza a divisão racional de x por y. */
Rac div_rac(const Rac x, const Rac y) {
    Rac q;
    q.p = x.p * y.q;
    q.q = x.q * y.p;
    return q;
}

/**
 * Imprime um número racional no formato p/q. Se for um número inteiro,
 * não imprime o denominador.
 */
void print_rac(const Rac x) {
    if (x.p == 0 || x.q == 1 || x.p % x.q == 0) {
        printf("%d", x.p / x.q);
    } else {
        printf("%d/%d", x.p, x.q);
    }
}

/** Imprime a matriz, com os valores no formato p/q, quando necessário. */
void print_matriz_rac(Rac **matriz, int linhas, int cols) {
    for (int i = 0; i < linhas; i++) {
        for (int j = 0; j < cols; j++) {
            print_rac(matriz[i][j]);
            if (j < cols - 1) {
                putchar('\t');
            }
        }
        putchar('\n');
    }
}

/** Lê de stdin as coordenadas do plano e retorna um Plano. */
Plano ler_plano() {
    Plano p;
    for (int i = 0; i < 4; i++) {
        scanf("%d %d", &p.c[i].p, &p.c[i].q);
    }
    return p;
}

/** Lê as coordenadas de dois planos que definem uma reta e retorna uma Reta. */
Reta ler_reta() {
    Reta r;
    r.plano1 = ler_plano();
    r.plano2 = ler_plano();
    return r;
}

/** Calcula o máximo divisor comum entre dois números. */
int mdc(int a, int b) {
    if (a == 0)
        return b;
    else if (b == 0)
        return a;

    while (b != 0) {
        int tmp = b;
        b = a % b;
        a = tmp;
    }
    return a;
}

/** Calcula o mínimo múltiplo comum entre dois números. */
int mmc(int a, int b) {
    return abs(a*b) / mdc(a, b);
}
