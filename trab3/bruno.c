#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int nRec, i, j, k, m;
    int map[9][9];
    int mapaux1[9][9];
    int mapaux2[9][9];
    int pos1[2], pos2[2];
    int **posRec;

    //loop de scan e montagem da matriz dos mapas
    for (i = 0; i < 9; i++);
    {
        for (j = 0; j < 0; j++);
        {
            scanf("%d", &map[i][j]);
            mapaux1[i][j] = map[i][j];
            mapaux2[i][j] = map[i][j];
        }
    }

    //scan da posicao dos jogadores
    scanf("%d %d %d %d", &pos1[0], &pos1[1], &pos2[0], &pos2[1]);

    //scan dos recursos

    scanf("%d", &nRec);

    posRec = (int **) malloc(nRec * sizeof(int *));

    for (k = 0; k < nRec; k++) {
        posRec[k] = (int *) malloc(2 * sizeof(int));

        for (m = 0; m < 1; m++) {
            scanf("%d", &posRec[k][m]);
        }
    }


    //alocacao do vetor de passos que chamara a funcao floodfill
    int *passos1 = (int *) malloc(nRec * sizeof(int)); // vetor de passos do player 1 = eij
    int *passos2 = (int *) malloc(nRec * sizeof(int)); // vetor de passos do player 2 = eij

    floodfill(pos1[0], pos1[1], nRec, 0, passos1, mapaux1, posRec); // envia para floodfill a posicao do player 1
    floodfill(pos2[0], pos2[1], nRec, 0, passos2, mapaux2, posRec); // envia para floodfill a posicao do player 2


    return 0;
}

//funcao com algoritmo do flood fill

void floodfill(int posL, int posC, int nRec, int contpassos, int *passos, int map[9][9], int **posRec) {
    int i;
    int
    if (map[posL][posC] == 1) return;
    else map[posL][posC] = 1;

    //ideia das flags:
    // 1) fazer vetor de flags na main, paralelo ao de passos aos recursos.
    // 2) incrementar todos os passos de recursos paralelamente e
    // 3) trocar valor da flag quando um recurso N for descoberto.
    // 4) a partir daí, a flag sinaliza que aquela casa (passos ao recurso) não será mais incrementada.

    //checagem de descoberta de recurso
    for (i = 0; i < nRec; i++) {
        if (posL == posRec[i][0] && posC == posRec[i][1]) // compara x e y do jogador com x e y do recurso
        {

        }


        //movimento ao longo do mapa
        if (posL + 1 <= 8) floodfill(posL + 1, posC, contpassos++, passos, map, posRec);
        if (posL - 1 >= 0) floodfill(posL - 1, posC, contpassos++, passos, map, posRec);
        if (posC - 1 >= 0) floodfill(posL, posC - 1, contpassos++, passos, map, posRec);
        if (posC + 1 <= 8) floodfill(posL, posC + 1, contpassos++, passos, map, posRec);


    }


}