project(trab3)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -pedantic -g -DDEBUG")

add_executable(main main.c)
target_link_libraries(main m)

add_executable(minimal minimal.c)
target_link_libraries(minimal m)

add_executable(bruno bruno.c)
target_link_libraries(bruno m)

add_executable(nathiele nathiele.c)
target_link_libraries(nathiele m)
