#include <stdio.h>
#include <stdlib.h>

#define maxMap 9
int** aloca_matriz (int linha, int coluna){             
    int** matriz;
       
    matriz = (int**) calloc(linha, sizeof(int*));          
	for (int i = 0; i < linha; i++) {
		matriz[i] = (int *) calloc(coluna, sizeof(int));   
	}
	
	return matriz;
	
}

void free_matriz (int n, int m, int** matriz){          
 
	for (int i = 0; i < n; i++) {
		free(matriz[i]);
	}
	free(matriz);
       
}

int qtd_transponivel(int matriz[maxMap][maxMap]){
    int qtd = 0;
    for(int i = 0; i<maxMap; i++){
        for(int j = 0; j<maxMap; j++){
            if (matriz[i][j] == 0){
                qtd++;
            }
        }
    }
    return qtd;
}

int verificaVisitado(int x, int y, int visitados[81][2], int qtd){

    for(int i = 0; i<qtd; i++){
        if(x == visitados[i][0] && y == visitados[i][1]){
            return 1;
        }
    }
    return 0;
    
}

int contaPassos_recursos(int matriz[maxMap][maxMap], int x, int y, int Xrec, int Yrec){
    int recurso = 0, passos = 1, ndeu = 0;
    int direcao = 1;            //1 sul, 2 norte, 3 esquerda, 4 direira
    int contador = 0;
    int visitados[81][2];
    
    visitados[0][0] = x;
    visitados[0][1] = y;

    while(recurso == 0){
        //printf("aqui\n");
        if(y == 0 && direcao == 1){                     //verifica se estou na primeira linha
            // não posso ir pra sul
            //printf("oi\n");
            direcao = 2;
        }
        else if(y == maxMap-1 && direcao == 2){                //verifica se estou na ultima linha
            // nao posso ir pra norte
            direcao = 3;
        }
        else if(x == 0 && direcao == 3){                //verifica se estou na primeira coluna
            // nao posso ir pra esquerda
            direcao = 4;
        }
        else if(x == maxMap-1 && direcao == 4){                //verifica se estou na última coluna
            //nao posso ir pra direita
            direcao = 1;
        }
        
        
        if(direcao == 1){
            if (y == 0){
                direcao = 2;
            }
            else if(matriz[y-1][x] == 0 && verificaVisitado(x, y-1, visitados, passos) == 0){
                y = y-1;
                direcao = 1;
                //printf("baixo\n");    
                visitados[passos][0] = x;
                visitados[passos][1] = y;
                passos++;
                ndeu = 0;
            }
            else{
                direcao = 2;
                ndeu++;
            }
            
        }
        
        else if(direcao == 2){
            if (y == maxMap-1){
                direcao = 3;
            }
            else if(matriz[y+1][x] == 0 && verificaVisitado(x, y+1, visitados, passos) == 0){
                y = y+1;
                direcao = 2;
                //printf("cima\n");    
                visitados[passos][0] = x;
                visitados[passos][1] = y;
                passos++;
                ndeu = 0;
            }
            else{
                direcao = 3;
                ndeu++;
            }
        }
        
       else if(direcao == 3){
            if (x == 0){
                direcao = 4;
            }
            else if(matriz[y][x-1] == 0 && verificaVisitado(x-1, y, visitados, passos) == 0){
                x = x-1;
                direcao = 3;
               // printf("esquerda\n");    
                visitados[passos][0] = x;
                visitados[passos][1] = y;
                passos++;
                ndeu = 0;
             }
             else{
                direcao = 4;
                ndeu++;
             }
                
       }
       
       else{
            if(x == maxMap-1){
                direcao = 1;
            }
            else if(matriz[y][x+1] == 0 && verificaVisitado(x+1, y, visitados, passos) == 0){
                x = x+1;
                direcao = 4;
                //printf("direita\n");    
                visitados[passos][0] = x;
                visitados[passos][1] = y;
                passos++;
                ndeu = 0;
            }
            else{
                direcao = 1;
                ndeu++;
            }
       }
        //printf("x: %d, y: %d, xr: %d, yr: %d\n\n", x, y, Xrec, Yrec);
        
        if(x == Xrec && y == Yrec){
            //printf("x: %d\n", x);
            //printf("y: %d\n", y);
            recurso = 1;        
        }
        else{
            recurso = 0;
        }
        
        if (ndeu == 4){             // se ndeu for = 4 quer dizer que ele tentou ir pras 4 direções e nao conseguiu, 
            contador++;
            x = visitados[passos-contador][0];
            y = visitados[passos-contador][1];
            ndeu = 0;
        }
        
    
    }
    

    return passos;

}

float calcula(int recursos, int transp, int passos[recursos]){
    float E = 0;
    float Et = 0;   
    for(int i = 0; i<recursos; i++){
        E = E + ((passos[i]*1.0)/(transp*1.0));
    }
    
    Et = (1.0/recursos)*E;
    return Et;
}

int main(){
    int map[maxMap][maxMap];
    int i = 0, j = 0, nRec;
    int coord1[2], coord2[2];               //Coordenadas jogadores 1 e 2
    
    for(i = 0; i<maxMap; i++){              //Lê conteúdo da matriz, locais in e transponíveis
        for(j = 0; j<maxMap; j++){
            scanf("%d", &map[i][j]);
        }

    }
    scanf("%d", &coord1[1]);                //lê coordenadas dos jogadores
    scanf("%d", &coord1[0]);
    scanf("%d", &coord2[1]);
    scanf("%d", &coord2[0]);

    scanf("%d", &nRec);
    
    int** posRec;                           //Matriz que armazena as posições dos recursos
    posRec = aloca_matriz(nRec, 2);
    
    for(i = 0; i<nRec; i++){                //Lê as posições
        scanf("%d",&posRec[i][1]);
        scanf("%d",&posRec[i][0]);
    }
    
   // printf("leu\n");
    int qtd = qtd_transponivel(map);
   // printf("n de transponiveis: %d\n", qtd);
    
    
    
    int passos1[nRec], passos2[nRec];
    
  /*   for(int b = 0; b<maxMap; b++){
        for(int a = 0; a<maxMap; a++){
            printf("%d ", map[b][a]);
        }
        printf("\n");
    }*/
    
   // printf("\n\n\n\n\n");
   for(i = 0; i<nRec; i++){
       passos1[i] = contaPassos_recursos(map, coord1[0], coord1[1], posRec[i][0], posRec[i][1]);
       passos2[i] = contaPassos_recursos(map, coord2[0], coord2[1], posRec[i][0], posRec[i][1]);
       printf("passos1: %d\n", passos1[i]);
       printf("passos2: %d\n", passos2[i]);
    }
    
    float vExplor1, vExplor2;
    vExplor1 = calcula(nRec, qtd, passos1);
    vExplor2 = calcula(nRec, qtd, passos2);
    printf("%f\n",vExplor1);
    printf("%f\n",vExplor2);
    
    if(vExplor1 < vExplor2 && (vExplor2 - vExplor1) > 0.001){
        printf("O jogador 1 possui vantagem\n");
    
    }
    else if(vExplor2 < vExplor1 && (vExplor1 - vExplor2) > 0.001){
        printf("O jogador 2 possui vantagem\n");   
    }
    else{
        printf("O mapa eh balanceado \n");
    }
    
    free_matriz(nRec, 2, posRec);
    return 0;
}
