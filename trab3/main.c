/*
 *     ____  ____  ____
 *    / __ \/ __ \/ __ \
 *   / /  \/ /  \/ /  \ \
 *   \ \  / /\  / /\  / /
 *    \ \/ /\ \/ /\ \/ /
 *     \ \/  \ \/  \ \/
 *     /\ \  /\ \  /\ \
 *    / /\ \/ /\ \/ /\ \
 *   / /  \/ /  \/ /  \ \
 *  / /   / /\  / /\   \ \
 *  | |  / / /  \ \ \  | |
 *  \ \__\/ /    \/ /__/ /
 *   \_____/\____/ /____/
 *         \______/
 *
 * Balanceamento de Recursos
 *
 * Autor: Bráulio Bezerra da Silva - BCC ICMC
 * Número USP: 10734540
 *
 * Trabalho 3 de ICC1 - 2018.1
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/** Tamanho fixo de cada dimensão do mapa. */
const int MAX_MAPA = 9;

/** Possíveis estados de uma posição no mapa. */
typedef enum _TipoPos {
    LIVRE = 0,
    BLOQUEADO = 1
} TipoPos;

/** Uma posição (x,y). */
typedef struct _Pos {
    int x, y;
} Pos;

// pré declarações para uso na main()

int ler_mapa (TipoPos map[MAX_MAPA][MAX_MAPA]);
double valor_exploracao(Pos inicio, Pos rec[], TipoPos map[MAX_MAPA][MAX_MAPA],
                        int n_recs, int n_transp, Pos base_oposta);
int espacos_explorados(Pos inicio, Pos fim  , TipoPos map[MAX_MAPA][MAX_MAPA],
                     bool vis[MAX_MAPA][MAX_MAPA]);

#ifdef DEBUG
    void mostra_mapa(TipoPos map[MAX_MAPA][MAX_MAPA], bool vis[MAX_MAPA][MAX_MAPA], Pos curr_pos, Pos dest);
#endif

int main() {

    // leitura do mapa

    TipoPos map[MAX_MAPA][MAX_MAPA];
    int n_transp = ler_mapa(map);

    // leitura das posições iniciais dos jogadores

    Pos pos1, pos2;
    scanf("%d %d %d %d", &pos1.x, &pos1.y, &pos2.x, &pos2.y);

    // leitura da quantidade e posições dos recursos

    int n_recs;
    scanf("%d", &n_recs);

    Pos* posRec = malloc(n_recs * sizeof(Pos));
    if (posRec == NULL) {
        fprintf(stderr, "Out of memory\n");
        return 1;
    }

    for (int i = 0; i < n_recs; i++) {
        scanf("%d %d", &posRec[i].x, &posRec[i].y);
    }

    // cálculo dos valores de exploração para os dois jogadores
    
    double valor_expl_1 = valor_exploracao(pos1, posRec, map, n_recs, n_transp, pos2);
    double valor_expl_2 = valor_exploracao(pos2, posRec, map, n_recs, n_transp, pos1);

    // impressão de resultados

    printf("%.6f\n", valor_expl_1);
    printf("%.6f\n", valor_expl_2);

    if (valor_expl_1 - valor_expl_2 >= 0.001) {
        puts("O jogador 2 possui vantagem");
    } else if (valor_expl_1 - valor_expl_2 <= -0.001) {
        puts("O jogador 1 possui vantagem");
    } else {
        puts("O mapa eh balanceado");
    }

    // liberação de memória

    free(posRec);

    return 0;
}

/**
 * Lê o mapa, colocando-o em `map`, e retorna a quantidade de posições transponíveis.
 * O significado das posições é o seguinte:
 *
 *  0 = posição LIVRE
 *  1 = posição BLOQUEADA
 */
int ler_mapa(TipoPos map[MAX_MAPA][MAX_MAPA]) {
    int total_transp = 0;
    
    for (int i = 0; i < MAX_MAPA; i++) {
        for (int j = 0; j < MAX_MAPA; j++) {
            scanf("%d", (int*) &map[i][j]);
            if (map[i][j] == LIVRE) {
                total_transp++;
            }
        }
    }

    return total_transp;
}

/**
 * Calcula o valor de exploração a partir da posição `início`, dados:
 *
 *  - o mapa `map[][]`
 *  - os recursos `rec[]`
 *  - o número recursos `n_recs`
 *  - o número `n_transp` pré-calculado de células transponíveis no mapa
 *  - a posição da `base_oposta` que pode ser visitada, mas não é considerada
 *    na contagem de espaços explorados
 *
 * A fórmula do cálculo é a seguinte:
 *
 *         1         E(i)
 *  V = ------ * ∑ --------
 *      n_recs     n_transp
 *
 * onde E(i) é a quantidade de espaços explorados, usando a função `espaços explorados`.
 */
double valor_exploracao(Pos inicio, Pos rec[], TipoPos map[MAX_MAPA][MAX_MAPA],
                       int n_recs, int n_transp, Pos base_oposta) {
    double total = 0;

    for (int i = 0; i < n_recs; i++) {
        bool vis[MAX_MAPA][MAX_MAPA];
        memset(vis, false, MAX_MAPA*MAX_MAPA);

        int result = espacos_explorados(inicio, rec[i], map, vis);
        if (vis[base_oposta.x][base_oposta.y]) {
#ifdef DEBUG
//            printf("-1 (passou pela base oposta)\n");
#endif
//            result -= 1; // FIXME só passou nos testes sem essa consideração
        }
#ifdef DEBUG
        printf("%d / %d = %.6lf\n", result, n_transp, (double)result / n_transp);
        mostra_mapa(map, vis, inicio, rec[i]);
#endif
        total += (double)result / n_transp;
    }

    return total / n_recs;
}

/**
 * Realiza a exploração do mapa `map` da posição `inicio` até a posição `fim` e retorna
 * a quantidade de células que foram exploradas. Utiliza a matriz `vis` para evitar
 * passar por posições que já foram visitadas.
 *
 * A exploração se comporta como um "flood fill" que:
 *
 *  - visita as posições na ordem Sul, Norte, Oeste, Leste
 *  - evita a visita a posições bloqueadas
 */
int espacos_explorados(Pos inicio, Pos fim, TipoPos map[MAX_MAPA][MAX_MAPA],
                       bool vis[MAX_MAPA][MAX_MAPA]) {

    vis[inicio.x][inicio.y] = true;

#ifdef DOUBLE_DEBUG
    mostra_mapa(map, vis, inicio, fim);
#endif

    if (inicio.x == fim.x && inicio.y == fim.y) {
        return 1;
    }

    Pos next[4] = {{1,0},{-1,0},{0,-1},{0,1}}; // sul, norte, oeste, leste

    int total = 0;

    for (int i = 0; i < 4; i++) {
        Pos p = {inicio.x + next[i].x, inicio.y + next[i].y};
        if (
                p.x < 0 || p.y < 0 ||
                p.x >= MAX_MAPA || p.y >= MAX_MAPA ||
                vis[p.x][p.y] ||
                map[p.x][p.y] == BLOQUEADO) {
            continue;
        }
        total += espacos_explorados(p, fim, map, vis);
        if (vis[fim.x][fim.y])
            break;
    }

    return total + 1;
}

#ifdef DEBUG
/** Imprime o mapa na tela, com propósitos de debug. */
void mostra_mapa(TipoPos map[MAX_MAPA][MAX_MAPA], bool vis[MAX_MAPA][MAX_MAPA],
                 Pos curr_pos, Pos dest) {
    printf("___________\n");
    for (int i = 0; i < MAX_MAPA; i++) {
        printf("|");
        for (int j = 0; j < MAX_MAPA; j++) {
            if (i == curr_pos.x && j == curr_pos.y) {
                printf("@");
            } else if (i == dest.x && j == dest.y) {
                printf("#");
            } else if (vis[i][j]) {
                printf(".");
            } else if (map[i][j] == BLOQUEADO) {
                printf("X");
            } else {
                printf(" ");
            }
        }
        puts("|");
    }
    puts("");
}
#endif

