#include <stdio.h>
#include <stdlib.h>
int floodfill(int xi, int yi, int xf, int yf, int m[9][9], int vis[9][9]) {
    vis[xi][yi] = 1;
    if (xi == xf && yi == yf) return 1;
    int total = 0, next[4][2] = {{1,0},{-1,0},{0,-1},{0,1}};
    for (int i = 0; i < 4; i++) {
		int xp = xi + next[i][0], yp = yi + next[i][1];
        if (! (xp < 0 || yp < 0 || xp >= 9 || yp >= 9 || vis[xp][yp] || m[xp][yp] == 1 || vis[xf][yf]))
        	total += floodfill(xp, yp, xf, yf, m, vis);
    }
    return total + 1;
}
double explr(int xi, int yi, int * rec, int m[9][9], int n_recs, int n_transp) {
    double total = 0;
    for (int i = 0; i < n_recs; i++) {
        int vis[9][9] = {{0},{0},{0},{0},{0},{0},{0},{0},{0}};
        total += (double) floodfill(xi, yi, rec[i*2+0], rec[i*2+1], m, vis) / n_transp;
    }
    return total / n_recs;
}
int main() {
    int n_transp, n_recs, m[9][9], xp1, yp1, xp2, yp2, *rec;
    for (int i = 0; i < 81; i++) {
        scanf("%d", &m[i / 9][i % 9]);
        n_transp += m[i / 9][i % 9] == 0;
    }
    scanf("%d %d %d %d %d", &xp1, &yp1, &xp2, &yp2, &n_recs);
    rec = (int*) malloc(2 * n_recs * sizeof(int));
    for (int i = 0; i < n_recs; i++) scanf("%d %d", &rec[i*2 + 0], &rec[i*2 + 1]);
    double val[2] = {explr(xp1, yp1, rec, m, n_recs, n_transp), explr(xp2, yp2, rec, m, n_recs, n_transp)};
    printf("%.6f\n%.6f\n", val[0], val[1]);
    if      (val[0] - val[1] >=  0.001) puts("O jogador 2 possui vantagem");
	else if (val[0] - val[1] <= -0.001) puts("O jogador 1 possui vantagem");
    else                                puts("O mapa eh balanceado");
    free(rec);
}

