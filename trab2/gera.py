#!/bin/python3

import random
import sys
import time

random.seed(int(time.time()*1000000))

usuarios = random.randint(4, 6)
filmes = random.randint(2, 6)
limiar = random.random()*0.5 + 0.5

print("{} {} {:.2}".format(usuarios, filmes, limiar))

for j in range(0, usuarios):
    soma = 0
    for k in range(0, filmes):
        n = random.randint(1, 5)
        if (random.random() < 0.15):
            n = 0
        soma += n
        sys.stdout.write("{} ".format(n))
    print()

