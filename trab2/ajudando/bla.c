#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main (void) {

	int n, i;
	float t;

	scanf("%d %d %f\n", &n, &i, &t);

	int matriz_notas [15][15];

	// zerando a MATRIZ_NOTAS	

	for ( int a = 0 ; a < n ; a++ ) {
		
		for ( int b = 0 ; b < i ; b++ ) {

			matriz_notas [a][b] = 0;

		}

	}

	// entrada da MATRIZ_NOTAS	

	for ( int a = 0 ; a < n ; a++ ) {
		
		for ( int b = 0 ; b < i ; b++ ) {

			scanf("%d", &matriz_notas [a][b] );

		}

	}

	// VETOR MEDIAS
	// trecho de codigo que calcula o vetor MEDIAS com N elementos, um por pessoa
	
	float medias [n];

	int a = 0; // representa a linha atual de MATRIZ_NOTAS
	int b = 0; // representa a coluna atual de MATRIZ_NOTAS
	int c; // representa a quantidade de valores !=0 usados no calculo da media, usado posteriormente como divisor

	for ( a = 0 ; a < n ; a++ ) {
		
		medias [a] = 0;	
		c = 0;	
		
		for ( b = 0 ; b < i ; b++ ) {
	
			medias [a] += matriz_notas [a][b]; 

			if (matriz_notas [a][b] != 0){

				c += 1;

			}

		}

		medias [a] = medias [a] / c;

	}

	// SIMILARIDADE
	// trecho de codigo que calcula todos os valores de similaridade
	// 'a' simboliza a linha da MATRIZ_SIM
	// 'b' simboliza a coluna da MATRIZ_SIM
	// 'c' simboliza a posicao do item 'i' (da MATRIZ_NOTAS) comum as duas pessoas 'a' e 'b' em questao

	float matriz_sim [n][n];

	// as declaracoes abaixo foram usadas como variaveis auxiliares para o calculo	
	
	float raiz_a = 0;		
	float raiz_b = 0;
	float dividendo_sim = 0;

	for ( a = 0 ; a < n ; a++ ) {

		for ( b = 0 ; b < n ; b++ ) {
			
			matriz_sim [a][b] = 0;

			if (a == b)
				continue;
			
			for ( int c = 0 ; c < i ; c++ ) {
				
				matriz_sim [a][b] += matriz_notas [a][c] * matriz_notas [b][c]; 
				raiz_a += pow (matriz_notas [a][c], 2);
				raiz_b += pow (matriz_notas [b][c], 2);
				
			}
			
			dividendo_sim = sqrt (raiz_a) * sqrt (raiz_b);
			matriz_sim [a][b] = matriz_sim [a][b] / dividendo_sim;

			// trecho que elimina os valores abaixo do limiar 't'1

//			if (matriz_sim [a][b] < t || matriz_sim [a][b] == 1) {
//
//				matriz_sim [a][b] = 0;
//			
//			}

			raiz_a = 0;
			raiz_b = 0;
			
		}

	}

	// NOTAS
	// trecho de codigo que calcula a predicao das notas '0'
	// a = linha MATRIZ NOTAS, linha MATRIZ SIM e coluna MEDIAS
	// b = coluna MATRIZ NOTA (filme 'i')
	// c = coluna MATRIZ SIM, S (outra linha MATRIZ NOTA + outra coluna MEDIA)

	float dividendo_notas = 0;
	float matriz_previsao [16][16];
	float soma_saidas [15] = {0};

	// zerando a MATRIZ_PREVISAO	

	for ( a = 0 ; a < n ; a++ ) {
		
		for ( b = 0 ; b < i ; b++ ) {

			matriz_previsao [a][b] = 0;

		}

	}

	for ( a = 0 ; a < n ; a ++ ) {
		
		for ( b = 0 ; b < i ; b ++ ) {
			
			if ( matriz_notas [a][b] == 0 ) { 

				for ( c = 0 ; c < n ; c ++ ) {

					if (c == a || matriz_sim[a][c] < t || matriz_notas[c][b] == 0)
						continue;

					matriz_previsao [a][b] += matriz_sim [a][c] * (matriz_notas [c][b] - medias [c]);
					dividendo_notas += matriz_sim [a][c];

				}
				
				matriz_previsao [a][b] /= dividendo_notas;
				matriz_previsao [a][b] += medias [a];
				dividendo_notas = 0;

				if ( matriz_previsao [a][b] > 0 ) {

					printf ("%.2f ", matriz_previsao [a][b]);

				} 

				else {
	
					printf ("DI ");
	
				}

			}
		
		
		}


		for ( int d = 0 ; d < i ; d ++ ) {
			 
			soma_saidas [a] += matriz_previsao [a][d];
		
		}
		
		if ( soma_saidas [a] != 0 ) {

			printf ("\n");

		}

		
	
	}
	

	return 0;
}

