#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main (){
    int npessoas; // numero de usuarios do sistema ( 4 ≤ npessoas ≤ 15)
    int itens; // numero de itens (filmes) na base de dados (1 ≤ itens ≤ 15)
    double limiar; // limiar que define a similaridade entre 2 usuarios (0.5 ≤ limiar ≤ 1.0)

    scanf("%d %d %lf", &npessoas, &itens, &limiar);

    //este bloco sera responsavel por escanear a matriz que armazena as notas dadas pelos usuarios
    int i;
    int j;
    int matnotas[npessoas][itens];

    for (i = 0; i < npessoas; i++)
    {
        for (j = 0; j < itens; j++)
        {
            matnotas[i][j] = 0;
            scanf("%d", &matnotas[i][j]);
        }
    }

    for (i = 0; i < npessoas; i++) // teste para confirmar o scan da matriz
    {
        for (j = 0; j < itens; j++)
        {
            printf("%d\n ", matnotas[i][j]);
        }
    }



    //o bloco a seguir calculara os valores que compoem as similaridades
    double uvx, modu, modv, sim;
    int k;

    sim = 0; //a similaridade em si

    double matsim[npessoas][npessoas]; //matriz que armazena os valores de similaridade


    for(i = 0; i < npessoas; i++) //estes 'for' aninhados fazem o papel de percorrer as notas na matscan de forma a avaliar as permutacoes possiveis de usuarios
    {
        for(j = 0; j < npessoas; j++)
        {
            double uvx = 0;  //numerador da formula da similaridade
            double modu = 0; //primeira parte do denominador da mesma formula
            double modv = 0; //segunda parte do denominador
            for (k = 0; k < itens; k++)
            {
                uvx = uvx + (matnotas[i][k] * matnotas[j][k]);
                modu = modu + pow(matnotas[i][k], 2);
                modv = modv + pow(matnotas[j][k], 2);
            }
            matsim[i][j] = uvx / (sqrt(modu)*sqrt(modv));
        }
    }

    for (int t1 = 0; t1 < npessoas; t1++)  //teste para imprimir as similaridades
    {
        for (int t2 = 0; t2 < npessoas; t2++)
        {
            printf("%.2lf ", matsim[t1][t2]);
        }
        printf("\n");
    }

    // o proximo bloco criara um vetor que armazena as medias das notas de cada usuario
    float vetmedia[npessoas];

    for(i = 0; i < npessoas; i++) //zerando o vetor
    {
        vetmedia[i] = 0;
    }




    for(i = 0; i < npessoas; i++)
    {
        for(j = 0; j < itens; j++)
        {
            vetmedia[i] = vetmedia[i] + matnotas[i][j];
        }

        vetmedia[i] = vetmedia[i]/j;

    }

    for(i = 0; i < npessoas; i++) //testando os valores de vetmedia
    {
        printf(" a media e %.2f\n", vetmedia[i]);
    }

    //o proximo bloco sera responsavel pela previsao da nota dos usuarios para os filmes que nao viram (nota 0 na matscan)

    int m, n;
    double somasim, var, prevnum, prevden, prev;

    somasim = 0;
    var = 0;
    prevnum = 0;
    prev = 0;



    for(i = 0; i < npessoas; i++)
    {
        for (j = 0; j < itens; j++)
        {
            if (matnotas[i][j] = 0)
            {

                for(m = 0; m < npessoas; m++)
                {
                    if(m == i)
                    {
                        m++;
                    }
                    if(matsim[i][m] > limiar)
                    {
                        var = matnotas[m][j] - vetmedia[m];
                        somasim = somasim + matsim[i][m];
                        prevnum = (matsim[i][m] * var);
                        prev = prevnum/somasim;
                    }
                }
                prev = vetmedia[i] + prev;
                printf("eis a prev %lf\n", prev);
            }


        }
    }






    return 0;



}


