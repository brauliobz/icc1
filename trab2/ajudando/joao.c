#include <stdio.h>
#include <math.h>
#include <stdbool.h>
int main()
{
    int user_linha, filme_col; // dimensão da matriz que armazena as notas
    float limiar;
    float notas[15][15] = {0}; // armazena as notas
    float similaridade[15][15] = {0};//armazena as similaridades(cos)
    float media[15] = {0}; // armazena as médias
    float num=0; // usado pra contas
    float den1=0; // usado pra contas
    float den2=0; // usado pra contas
    bool quebra_de_linha=false;//define se vai ter ou não quebra de linha na saída
    scanf("%d%d%g", &user_linha, &filme_col, &limiar);










//Bloco 1 recebe as notas =============================================================================
    for (int linha = 0; linha < user_linha; ++linha)
    {
        for (int coluna = 0; coluna < filme_col  ; ++coluna)
        {
            scanf("%f", &notas[linha][coluna]);
        }
    }

//Bloco 2 calcula a similaridade ===========================================================================
//funciona comparando um usuário(que é a linha_a) a outro(linha_b), o primeiro e o segundo for servem pra manejar esses dois
//num, den1, den2, são variáveis que servem pra guardar valores e fazer contas temporariamente e então são zeradas para serem usadas denovo
    for (int linha_a = 0; linha_a < user_linha; ++linha_a)
    {

        for (int linha_b = linha_a + 1; linha_b < user_linha; ++linha_b)
        {
            num=0;
            den1=0;
            den2=0;
            for (int coluna = 0; coluna < filme_col; ++coluna)
            {
                num = num + (notas[linha_a][coluna] * notas[linha_b][coluna]);
                den1 = den1 + (notas[linha_a][coluna] * notas[linha_a][coluna]);
                den2 = den2 + (notas[linha_b][coluna] * notas[linha_b][coluna]);
            }

            similaridade[linha_a][linha_b] = num / ( sqrt(den1) * sqrt(den2) );
            similaridade[linha_b][linha_a] = num / ( sqrt(den1) * sqrt(den2) );
        }
    }

//Bloco 3 calculo das media ===============================================================================================

    for (int linha = 0; linha < user_linha ; ++linha)
    {	num=0;
        den1=0;
        for (int coluna = 0; coluna < filme_col; ++coluna)
        {
            if(notas[linha][coluna] != 0)
            {
                num = num + notas[linha][coluna];
                den1 = den1 + 1;
            }
        }
        media[linha] = num / den1;
    }


//Bloco 4 predição ==================================================================================================
// checa usuario por usuario, caso ache 0(primeiro if) procura os usuarios com similaridade maior que a limiar(segundo if) e com notas maior que 0(segundo if) para predizer a nota
//usuario_lin controla o usuario que está sendo checado
//filme_colu controla o filme que está sendo checado e col_lin controla tanto as colunas da matriz similaridade, quanto as linhas da matriz nota
//quebra_de_linha define quando vai haver a quebra de linha
    for (int usuario_lin = 0; usuario_lin < user_linha; ++usuario_lin)
    {
        for (int filme_colu = 0; filme_colu < filme_col; ++filme_colu)
        {
            num=0;
            den1=0;
            if (notas[usuario_lin][filme_colu] == 0)
            {
                for (int col_lin = 0; col_lin < user_linha; ++col_lin)
                {
                    if (similaridade[usuario_lin][col_lin] >= limiar && notas[col_lin][filme_colu] != 0)
                    {

                        num = num + similaridade[usuario_lin][col_lin] * ( notas[col_lin][filme_colu] - media[col_lin] );
                        den1 = den1 + similaridade[usuario_lin][col_lin];
                    }
                }

                if (media[usuario_lin] + (num / den1) > 0)
                {
                    printf("%.2f ", media[usuario_lin] + (num / den1) );
                    quebra_de_linha = true;
                }
                else
                    printf("DI ");
                quebra_de_linha = true;
            }
        }
        if(quebra_de_linha == true)
        {
            printf("\n");
        }
        quebra_de_linha = false;

    }

    return 0;
}