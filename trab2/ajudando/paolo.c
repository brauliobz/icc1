#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    //primeiro a declaração das variaveis junto da leitura dos primeiros termos
    int numUsuarios, itensBaseDeDados, cossenoNumerador, k, y, colunas, cosseno, den1, den2, valoresNaoNulos[255], denominador, numerador;
    colunas = 0;
    valoresNaoNulos += 0;
    float limiarDeProximidade, parte1den, parte2den, cossenoDenominador, notaNumerador, notaDenominador, nota, somadorNotas, mediaNotas, tabelaSimilaridade[numUsuarios][itensBaseDeDados], media;
    scanf("%d %d %f", &numUsuarios, &itensBaseDeDados, &limiarDeProximidade);
    int tabelaEntrada[numUsuarios][itensBaseDeDados];

    if(numUsuarios < 4 || numUsuarios > 15){
        return 0;
    } else {
        if(itensBaseDeDados < 4 || numUsuarios > 15){
            return 0;
        } else {

            for(int j = 0; j < numUsuarios; j++){
                for(int k = 0; k < itensBaseDeDados; k++){
                    tabelaEntrada[j][k] = 0;
                }
            }

            //agora colocaremos as informações em seus respectivos lugares
            for(int j = 0; j < numUsuarios; j++){
                for(int k = 0; k < itensBaseDeDados; k++){
                    scanf("%d", &tabelaEntrada[j][k]);
                }
            }

            //calculando a similaridade
            for(int i = 0; i < numUsuarios; i++){
                for(int u = 0; u < numUsuarios; u++){
                    for(int g = 0; g < itensBaseDeDados; g++){
                        cossenoNumerador += tabelaEntrada[i][g] * tabelaEntrada[u][g];
                        den1 += pow(tabelaEntrada[i][g],2);
                        den2 += pow(tabelaEntrada[u][g],2);
                        cossenoDenominador = sqrt(den1 * den2);
                    }
                    cosseno = cossenoNumerador/cossenoDenominador;
                    tabelaSimilaridade[i][u] = cosseno;
                }
            }

            //calculando as notas
            for(int m = 0; m < numUsuarios; m++){
                for(int n = 0; n < itensBaseDeDados; n++){
                    if(tabelaEntrada[m][n] == 0){
                        for(int h = 0; h < numUsuarios; h++){
                            if(tabelaEntrada[h][n] != 0){
                                denominador += 1;
                            }
                            for(int w = 0; w < itensBaseDeDados; w++){
                                numerador += tabelaEntrada[m][w];
                            }
                            media = numerador/denominador;
                        }
                    }
                }
            }
        }

    }




























































































    if(limiarDeProximidade < 0.5 || limiarDeProximidade > 1.0){
        return 0;
    } else {
        for(int i = 0; i < numUsuarios; i++){
            for(int u = 0; u < itensBaseDeDados; u++){
                if(i != numUsuarios){
                    for(int x = 0; x < itensBaseDeDados; x++){
                        k = i + 1;
                        cossenoNumerador += tabelaEntrada[i][x] * tabelaEntrada[k][x];
                    }
                } else {
                    for(int x = 0; x < itensBaseDeDados; x++){
                        k = i - 1;
                        cossenoNumerador += tabelaEntrada[i][x] * tabelaEntrada[k][x];
                    }
                }

                for(int x = 0; x < itensBaseDeDados; x++){
                    if(i != numUsuarios){
                        y = i + 1;
                        parte1den += pow(tabelaEntrada[i][x], 2);
                        parte2den += pow(tabelaEntrada[y][x], 2);
                    } else {
                        y = i - 1;
                        parte1den += pow(tabelaEntrada[i][x], 2);
                        parte2den += pow(tabelaEntrada[y][x], 2);
                    }
                    cossenoDenominador = sqrt(parte1den) * sqrt(parte2den);
                }
                cosseno = cossenoNumerador/cossenoDenominador;
                if(cosseno < limiarDeProximidade){
                    tabelaSimilaridade[i][colunas] += cosseno;
                }
                else {
                    tabelaSimilaridade[i][colunas] = 0;
                }
            }
        }
    }

    for(int i = 0; i < numUsuarios; i++){
        for(int u = 0; u < itensBaseDeDados; u++){
            if(tabelaEntrada[i][u] == 0){
                for(int z = 0; z < numUsuarios; z++){
                    notaNumerador += tabelaSimilaridade[z][colunas] * tabelaEntrada[z][itensBaseDeDados];
                }
                for(int w = 0; w < numUsuarios; w++){
                    notaDenominador += tabelaSimilaridade[w][colunas];
                }
                for(int t = 0; t< itensBaseDeDados; t++){
                    somadorNotas += tabelaEntrada[i][t];
                }

                mediaNotas = somadorNotas / itensBaseDeDados;
                nota = mediaNotas + (notaNumerador/notaDenominador);

                if(nota < 0 || nota > 5){
                    printf("DI\n");
                } else {
                    printf("%f", nota);
                }
            }
        }
    }
}
}
return 0;
}

