//		*SISTEMAS DE RECOMENDAÇÃO*
//		Maurílio da Motta Meireles

#include<stdio.h>
#include<math.h>

int main() {

    int n_usuarios, n_filmes;
    double limiar;
    scanf("%d %d %lf", &n_usuarios, &n_filmes, &limiar);

    int nota[n_usuarios][n_usuarios];
    int usu1, filme;

    double media[n_usuarios];
    double soma_pra_media[n_usuarios];
    double div_pra_media;

    double nota_descoberta[n_usuarios][n_filmes];

    for (usu1 = 0; usu1 < n_usuarios; usu1++) {
        soma_pra_media[usu1] = 0;
        div_pra_media = 0;
        for (filme = 0; filme < n_filmes; filme++) {
            nota_descoberta[usu1][filme] = 0;

            scanf(" %d", &nota[usu1][filme]);

            if (nota[usu1][filme] != 0) {
                soma_pra_media[usu1] += (double) nota[usu1][filme];
                div_pra_media++;
            }
        }
        media[usu1] = soma_pra_media[usu1] / div_pra_media;
    }


    double sim[n_usuarios][n_usuarios];
    int u, v, k;
    double escalar, somaoq_u, somaoq_v;

    for (u = 0; u < n_usuarios; u++) {
        for (v = 0; v < n_usuarios; v++) {
            if (v > u) {

                escalar = 0;
                somaoq_u = 0;
                somaoq_v = 0;

                for (k = 0; k < n_filmes; k++) {
                    escalar = escalar + (nota[u][k] * nota[v][k]);
                    somaoq_u = somaoq_u + (nota[u][k] * nota[u][k]);
                    somaoq_v = somaoq_v + (nota[v][k] * nota[v][k]);
                }
                sim[u][v] = (double) escalar / (sqrt(somaoq_u * somaoq_v));
                sim[v][u] = sim[u][v];
                printf("%.2lf ", sim[u][v]);
            }
        }
        puts("");
    }

    for (int usu1 = 0; usu1 < n_usuarios; usu1++) {
        for (filme = 0; filme < n_filmes; filme++) {
            if (nota[usu1][filme] == 0) {

                double som_simedif = 0;
                double soma_sim = 0;

                for (int usu2 = 0; usu2 < n_usuarios; usu2++) {
                    if (usu2 != usu1 && sim[usu1][usu2] >= limiar && nota[usu2][filme] != 0) {
                        som_simedif += (sim[usu1][usu2] * (nota[usu2][filme] - media[usu2]));
                        soma_sim += sim[usu1][usu2];
                    }
                }
                if (soma_sim != 0) {
                    nota_descoberta[usu1][filme] = media[usu1] + (som_simedif / soma_sim);
                } else {
                    nota_descoberta[usu1][filme] = -1;
                }
            }
        }
    }

    char printei_antes;

    for (usu1 = 0; usu1 < n_usuarios; usu1++) {
        printei_antes = 'n';
        for (filme = 0; filme < n_filmes; filme++) {
            if (nota_descoberta[usu1][filme] != 0) {
                if (nota_descoberta[usu1][filme] == -1) printf("DI ");
                else printf("%.2lf ", nota_descoberta[usu1][filme]);
                printei_antes = 's';
            }
        }
        if (printei_antes == 's')printf("\n");
    }

    return 0;
}
