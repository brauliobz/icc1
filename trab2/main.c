/*
 *     ____  ____  ____
 *    / __ \/ __ \/ __ \
 *   / /  \/ /  \/ /  \ \
 *   \ \  / /\  / /\  / /
 *    \ \/ /\ \/ /\ \/ /
 *     \ \/  \ \/  \ \/
 *     /\ \  /\ \  /\ \
 *    / /\ \/ /\ \/ /\ \
 *   / /  \/ /  \/ /  \ \
 *  / /   / /\  / /\   \ \
 *  | |  / / /  \ \ \  | |
 *  \ \__\/ /    \/ /__/ /
 *   \_____/\____/ /____/
 *         \______/
 *
 * Sistema de Recomendação de Filmes
 *
 * Autor: Bráulio Bezerra da Silva - BCC ICMC
 * Número USP: 10734540
 *
 * Trabalho 2 de ICC1 - 2018.1
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#define MAX_USUARIOS (16)
#define MAX_FILMES   (16)

/** Guarda os dados utilizados para a recomendação de filmes. */
typedef struct Dados_ {

	int qtd_usuarios, qtd_filmes;

    /** Notas dadas pelos usuários para os filmes. Para filmes não avaliados é preenchido com 0 (zero). */
	int nota[MAX_USUARIOS][MAX_FILMES]; // usuario x filme.

    /** Média das notas dadas por um usuário. */
    double media[MAX_USUARIOS];

    /** Limiar de similaridade a partir do qual dois usuários são considerados na estimação de notas */
    double limiar;

    /** Medida da similaridade entre dois usuários, baseada nas notas dadas por eles. */
	double similaridade[MAX_USUARIOS][MAX_USUARIOS];

} Dados;

/**
 * Lê as entradas (qtd de usuários, filmes, limiar e notas) a partir de 'stdin'
 * e as armazena em 'dados'.
 */
void ler_entradas(Dados* dados) {
    int lidos = scanf("%d %d %lf", &dados->qtd_usuarios, &dados->qtd_filmes, &dados->limiar);

    bool ok = true;

    if (lidos != 3) {
        fprintf(stderr, "Formato inválido ao ler quantidade de usuários, de filmes e limiar.\n");
        ok = false;
    } else {
        if (dados->qtd_usuarios < 4 || dados->qtd_usuarios > 15) {
            fprintf(stderr, "Número de usuários deve estar entre 4 e 15.\n");
            ok = false;
        }
        if (dados->qtd_filmes < 1 || dados->qtd_filmes > 15) {
            fprintf(stderr, "Número de filmes deve estar entre 1 e 15.\n");
            ok = false;
        }
        if (dados->limiar < 0.5 || dados->limiar > 1.0) {
            fprintf(stderr, "Limiar deve ser de 0.5 a 1.0 .\n");
            ok = false;
        }
    }

    if (!ok) {
        exit(2);
    }

    for (int usu = 0; usu < dados->qtd_usuarios; usu++) {
        for (int filme = 0; filme < dados->qtd_filmes; filme++) {
            lidos = scanf("%d", &dados->nota[usu][filme]);
            if (lidos != 1) {
                fprintf(stderr, "Formato inválido ao ler nota[%d][%d].\n", usu, filme);
                exit(3);
            } else if (dados->nota[usu][filme] < 0 || dados->nota[usu][filme] > 5) {
                fprintf(stderr, "Nota[%d][%d] inválida: deve estar entre 0 e 5.\n", usu, filme);
                exit(4);
            }
        }
    }
}

/**
 * Calcula as médias de notas de cada usuário, ignorando notas zero, e as
 * armazena em 'dados.media'.
 */
void calcular_medias_individuais(Dados* dados) {
    for (int usu = 0; usu < dados->qtd_usuarios; usu++) {
        double total = 0.0;
        double filmes_com_nota = 0;
        for (int filme = 0; filme < dados->qtd_filmes; filme++) {
            if (dados->nota[usu][filme] != 0) {
                total += dados->nota[usu][filme];
                filmes_com_nota += 1;
            }
        }
        if (filmes_com_nota > 0) {
            dados->media[usu] = total / filmes_com_nota;
        } else {
            dados->media[usu] = 0.0;
        }
    }
}

/**
 * Retorna a norma ∥u∥ do vetor 'u'.
 *
 * @param dimensao a quantidade de elementos de 'u'
 */
double norma(const int u[], int dimensao) {
    double soma_quadrados = 0.0;
    for (int i = 0; i < dimensao; i++) {
        soma_quadrados += u[i]*u[i];
    }
    return sqrt(soma_quadrados);
}

/**
 * Calcula o produto escalar u⋅v.
 *
 * @param dimensao a quantidade de elementos nos vetores 'u' e 'v'
 */
double produto_escalar(const int u[], const int v[], int dimensao) {
    double resultado = 0.0;
    for (int i = 0; i < dimensao; i++) {
        resultado += u[i]*v[i];
    }
    return resultado;
}

/**
 * Calcula o cosseno entre dois vetores, dado pela fórmula:
 *
 *                    u ⋅ v
 *   cosseno(u, v) = ———————
 *                   ∥u∥ ∥v∥
 *
 * onde:
 *  - u⋅v é o produto escalar de u e v
 *  - ∥u∥ é a norma de u
 *
 * @param dimensao a quantidade de elementos nos vetores 'u' e 'v'
 */
double cosseno(const int u[], const int v[], int dimensao) {
    double norma_a = norma(u, dimensao);
    double norma_b = norma(v, dimensao);
    if (norma_a == 0 || norma_b == 0) {
        fprintf(stderr, "divisao por zero");
        exit(1);
    }

    return produto_escalar(u, v, dimensao) / (norma_a * norma_b);
}

/**
 * Calcula uma estimativa de nota de um filme para um usuário, baseado nas
 * notas dos usuários que têm notas similares a esse (similaridade >= limiar).
 * A fórmula utilizada é a seguinte:
 *
 *                              ∑ similaridade_i ⨯ (nota_i - média_i)
 * estimativa_nota = média_u + ———————————————————————————————————————
 *                                         ∑ |similaridade_i|
 *
 * onde:
 *  - média_u = média do usuário em questão
 *  - similaridade_i = similaridade com o usuário i
 *  - média_i = média de notas do usuário i
 *  - nota_i = nota que o usuário i deu ao filme
 *  - somente entram na conta os usuários que avaliaram o filme e que têm
 *    similaridade suficiente
 *
 * Retorna 0.0 quando não há como estimar nota por falta de notas e/ou usuários
 * similares.
 */
double estimar_nota(int usuario, int filme, Dados *dados) {
    double soma_notas_outros = 0.0;
    double soma_similaridades = 0.0;
    bool algum_no_limiar = false;
    for (int outro = 0; outro < dados->qtd_usuarios; outro++) {
        if (outro == usuario || dados->nota[outro][filme] == 0)
            continue;
        if (dados->similaridade[usuario][outro] - dados->limiar < 1e-10)
            continue;

        algum_no_limiar = true;
        soma_similaridades += fabs(dados->similaridade[usuario][outro]);
        soma_notas_outros +=
                dados->similaridade[usuario][outro]
                * (dados->nota[outro][filme] - dados->media[outro]);
    }

    if (algum_no_limiar) {
        return dados->media[usuario] + soma_notas_outros / soma_similaridades;
    } else {
        return 0.0;
    }
}

/**
 * Calcula as similaridades entre os usuários. O valor para cada par de usuários
 * é dado pelo cosseno dos vetores de notas dadas pelos dois usuários em questão.
 * As similaridades são armazenadas em 'dados.similaridade'.
 */
void calcular_similaridades(Dados* dados) {
    for (int u1 = 0; u1 < dados->qtd_usuarios; u1++) {
        dados->similaridade[u1][u1] = 0.0;
        for (int u2 = 0; u2 < u1; u2++) {
            dados->similaridade[u1][u2] = cosseno(dados->nota[u1], dados->nota[u2], dados->qtd_filmes);
            dados->similaridade[u2][u1] = dados->similaridade[u1][u2];

        }
    }
}

/**
 * Calcula e imprime em 'stdout' as notas estimadas para os filmes que os usuários
 * não avaliaram. Para as situações nas quais não foi possível estimar uma nota, é
 * impresso 'DI' (Dados Insuficientes).
 */
void estimar_imprimir_notas(Dados* dados) {
    for (int usu = 0; usu < dados->qtd_usuarios; usu++) {
        bool algum_sem_nota = false;
        for (int filme = 0; filme < dados->qtd_filmes; filme++) {
            if (dados->nota[usu][filme] == 0) {
                if (!algum_sem_nota) {
                    algum_sem_nota = true;
                } else {
                    putchar(' ');
                }
                double nota = estimar_nota(usu, filme, dados);
                if (nota != 0.0) {
                    printf("%.2lf", nota);
                } else {
                    printf("DI");
                }
            }
        }
        if (algum_sem_nota) {
            putchar('\n');
        }
    }
}

int main() {
	Dados dados;

	ler_entradas(&dados);
	calcular_medias_individuais(&dados);
	calcular_similaridades(&dados);
	estimar_imprimir_notas(&dados);

	return 0;
}

